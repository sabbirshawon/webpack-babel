# install webpack & babel

<code>
  
    npm init-y

</code>

<code>
  
    code .
    
</code>
<code>
  
     npm install --save-dev @babel/core @babel/preset-env

</code><br>
<br>
create a file name: <b>.babelrc</b>
<br>
<code>
  
    {
      "presets":[
         "babel-preset-env"
      ]
    }
</code>
<br>
<code>
  
     npm install --save-dev webpack babel-loader

</code>
<br>
<code>
  
    npm install --save-dev webpack-cli -D

</code><br>
<br>
create a file name: webpack.config.js
<br>
<code>
  
    const path = require('path');
  
    const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
     }
    }
    
    module.exports = config
    
</code>

